##### PatrickNByrne.com

### Adding new content

To add projects, add new entries to ```data/projects.yml```

Icons for these entries can be found on the font awesome icon list: http://fontawesome.io/icons/

### Customizing the front page

The front page is built with an HTML layout page. To modify it, edit ```layouts/index.html```

### Edit the menu bar

The menu bar entries can be edited in ```config.toml```

### Live preview the site

Run 

    hugo server

Open a web browser and navigate to http://localhost:1313

### Build the site

    hugo

### Deploy

After building the site, deploy the ```public/``` folder with any static webserver. 


